function createLayout(layoutName, cardBoard) {
    if (layoutName !== 'default') {
        console.log("Warning: I don't know layout", layoutName);
    }

    const leftGroup = document.createElement("div");
    leftGroup.className = "left-group";
    const card5ImgElement = document.createElement("img");
    card5ImgElement.id = "card-5";
    leftGroup.appendChild(card5ImgElement);

    const centerCardsElement = document.createElement("div");
    centerCardsElement.className = "center-cards";
    const card4ImgElement = document.createElement("img");
    card4ImgElement.id = "card-4";
    centerCardsElement.appendChild(card4ImgElement);
    const cardStackElement = document.createElement("div");
    cardStackElement.className = "card-stack";
    [1, 2].forEach(function(i) {
        const cardImgElement = document.createElement("img");
        cardImgElement.id = "card-" + i;
        cardStackElement.appendChild(cardImgElement);
    });
    centerCardsElement.appendChild(cardStackElement);
    const card6ImgElement = document.createElement("img");
    card6ImgElement.id = "card-6";
    centerCardsElement.appendChild(card6ImgElement);
    leftGroup.appendChild(centerCardsElement);

    const card3ImgElement = document.createElement("img");
    card3ImgElement.id = "card-3";
    leftGroup.appendChild(card3ImgElement);

    const rightGroup = document.createElement("div");
    rightGroup.className = "right-group";
    for (let i = 7; i <= 10; i++) {
        const cardImgElement = document.createElement("img");
        cardImgElement.id = "card-" + i;
        rightGroup.appendChild(cardImgElement);
    }

    cardBoard.appendChild(leftGroup);
    cardBoard.appendChild(rightGroup);
}

function shuffleDeck(deck) {
  for (let i = deck.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [deck[i], deck[j]] = [deck[j], deck[i]];
  }
  for (let i = 0; i < deck.length; i++) {
    deck[i].orientation = Math.round(Math.random());
  }

  return deck;
}

document.getElementById('start-btn').addEventListener('click', function() {
  const deckName = document.getElementById('deck-select').value;
  const layoutName = document.getElementById('card-layout').value;
  const selectionUi = document.getElementById('selection-ui');
  const readingUi = document.getElementById('reading-ui');
  const cardBoard = document.getElementById('card-placement');
  const nextCardBtn = document.getElementById('nextcard-btn');

  const xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.status === 200 && xmlhttp.readyState === 4) {
      const deckJSON = JSON.parse(xmlhttp.responseText);
      const deck = shuffleDeck(deckJSON.deck);

      createLayout(layoutName, cardBoard);
      selectionUi.style.display = 'none';
      readingUi.style.display = '';

      nextCardBtn.addEventListener('click', function() {
        placeCard(deckName, layoutName, deck, currentCard);
        currentCard++;
      }, false);
    }
  };
  xmlhttp.open('GET', './decks/' + deckName + '/DeckInfo.json');
  xmlhttp.send();
}, false);
