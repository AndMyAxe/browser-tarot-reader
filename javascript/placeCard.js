var placeCard = function (deckName, layout, deck, currentCard) {
  const currentCardId = `card-${currentCard}`;

  const cardElement = document.getElementById(currentCardId);
  if (!cardElement) {
    console.warn("Couldn't find card ${currentCard}! You have probably shown them all!");
    return;
  }
  // The source of the card
  cardElement.src = `./decks/${deckName}/${deck[currentCard].filename}`;
  // Set the alt text to the name of the card
  cardElement.alt = `${deck[currentCard].name}`;
  // Set the card style based on the card number
  cardElement.className = `${layout}-card-${currentCard}`;
  // If the card is inverted than display the image inverted
  if (deck[currentCard].orientation) {
    cardElement.className += ' inverted';
  }
}
