# Browser Tarot Reader

This is the first project for the And My Axe group.

It is going to be a simple in-browser tarot card reader.

The initial description of what the project needs:

I (@inmysocks) would like to allow multiple decks.

Deck
- 72 cards

Card
- Title
- Suit
- Value
- Definition
- Inverse definition
- Graphic

Shuffle
- Randomize cards
- Secondary randomization to "inverse"

Layout
- Card placement
 - Placement 1 definition_label
 - Placement 2 definition_label
 - Placement 3 definition_label
 - Placement n definition_label

Output
- Read verbatim card definitions
- Allow users to interpret meanings.


Other notes:

What format do we want the cards in? SVG is simple (and I have a set of images ready to use) but we should probably also support other types.

How much of this is going to be just html and css and how much javascript or ?
